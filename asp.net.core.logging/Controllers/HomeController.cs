﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using asp.net.core.logging.Models;

namespace asp.net.core.logging.Controllers
{
    public class HomeController : Controller
    {
        //private readonly ILogger<HomeController> _logger;
        private readonly ILogger _logger;
        private IMyClass _myClass;

        public HomeController(ILogger<HomeController> logger, IMyClass myClass)
        {
            _logger = logger;
            _myClass = myClass;
        }

        public IActionResult Index()
        {
            _logger.LogTrace("Simple log Message");
            _logger.LogDebug("Simple log Message");
            _logger.LogInformation("Simple log Message");
            _logger.LogWarning("Simple log Message");
            _logger.LogError("Simple log Message");
            _logger.LogCritical("Simple log Message");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public IActionResult Calculate(int a, int b) 
        {
            try
            {
               var result = a / b;
                ViewBag.Result = result;
               return View("Index", ViewBag.Result);
            }
            catch (Exception e )
            {
                _logger.LogError(e.Message);
                return null;
            }
          
        }
    }
}
