﻿using Microsoft.Extensions.Logging;

namespace asp.net.core.logging.Controllers
{
    public class MyClass : IMyClass
    {
        private readonly ILogger _logger;
        public MyClass(ILogger<MyClass> logger)
        {
            _logger = logger;
            _logger.LogWarning("MyClass Warning");
            _logger.LogTrace("Simple log Message");
            _logger.LogDebug("Simple log Message");
            _logger.LogInformation("Simple log Message");
            _logger.LogWarning("Simple log Message");
            _logger.LogError("Simple log Message");
            _logger.LogCritical("Simple log Message");
        }
    }
}
